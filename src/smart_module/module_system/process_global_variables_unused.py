from smart_module.module_system.module_info import *
from smart_module.module_system.process_common import *
from smart_module.module_system.process_operations import *

from smart_module.smart_core.base.executor.smart_module_config_parser import configParser
src_dir = configParser.getSrcDir()
export_dir = configParser.getExportDir()

def processGlobalVariablesUnused():
  print( "Checking global variable usages...")
  variable_uses = []
  variables = load_variables(export_dir,variable_uses)
  i = 0
  while (i < len(variables)):
    if (variable_uses[i] == 0):
      print( "WARNING: Global variable never used: " + variables[i])
    i = i + 1
