from smart_module.module_system.process_animations import processAnimations
from smart_module.module_system.process_dialogs import processDialogs
from smart_module.module_system.process_factions import processFactions
from smart_module.module_system.process_game_menus import processGameMenus
from smart_module.module_system.process_global_variables import processGlobalVariables
from smart_module.module_system.process_global_variables_unused import processGlobalVariablesUnused
from smart_module.module_system.process_info_pages import processInfoPages
from smart_module.module_system.process_init import processInit
from smart_module.module_system.process_items import processItems
from smart_module.module_system.process_map_icons import processMapIcons
from smart_module.module_system.process_meshes import processMeshs
from smart_module.module_system.process_mission_tmps import processMissionTmps
from smart_module.module_system.process_music import processMusic
from smart_module.module_system.process_particle_sys import processParticleSys
from smart_module.module_system.process_parties import processParties
from smart_module.module_system.process_party_tmps import processPartyTmps
from smart_module.module_system.process_postfx import processPostfx
from smart_module.module_system.process_presentations import processPresentations
from smart_module.module_system.process_quests import processQuests
from smart_module.module_system.process_scene_props import processSceneProps
from smart_module.module_system.process_scenes import processScenes
from smart_module.module_system.process_scripts import processScripts
from smart_module.module_system.process_simple_triggers import processSimpleTriggers
from smart_module.module_system.process_skills import processSkills
from smart_module.module_system.process_skins import processSkins
from smart_module.module_system.process_sounds import processSounds
from smart_module.module_system.process_strings import processStrings
from smart_module.module_system.process_tableau_materials import processTableauMaterials
from smart_module.module_system.process_tags_unused import processTagsUnused
from smart_module.module_system.process_troops import processTroops
from smart_module.smart_core.process_smart_module import processSmartModule
from smart_module.smart_core.report_smart_module import reportSmartModule
from smart_module.smart_core.base.executor.smart_module_config_parser import configParser
from smart_module.smart_core.smart_modules_log import info

################################################
###
### 用于执行脚本编译的主类，相当于【build_module.bat】功能
###
################################################


## 是否跳过原生编译
skipNative = configParser.getSkipNative()

## 预先处理SmartModule模块功能
processSmartModule()


if skipNative == False:
    info("现在开始进行原生系统的编译")
    ## 系统功能模块
    processTagsUnused()
    processInit()
    processGlobalVariables()
    processStrings()
    processSkills()
    processMusic()
    processAnimations()
    processMeshs()
    processSounds()
    processSkins()
    processMapIcons()
    processFactions()
    processItems()
    processScenes()
    processTroops()
    processParticleSys()
    processSceneProps()
    processTableauMaterials()
    processPresentations()
    processPartyTmps()
    processParties()
    processQuests()
    processInfoPages()
    processScripts()
    processMissionTmps()
    processSimpleTriggers()
    processDialogs()
    processGlobalVariablesUnused()
    processPostfx()
    processGameMenus()

## 显示概要信息
reportSmartModule()

