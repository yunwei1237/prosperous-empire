from smart_module.smart_core.utils.pythonUtils import mergeList
from smart_module.smart_modules.base.AiBaseScripts import aiBaseScripts
from smart_module.smart_modules.base.FactionBaseScripts import factionBaseScripts
from smart_module.smart_modules.base.MissionBaseScripts import missionBaseScripts
from smart_module.smart_modules.base.PartyBaseScripts import partyBaseScripts
from smart_module.smart_modules.base.StringBaseScripts import stringBaseScripts
from smart_module.smart_modules.base.TroopBaseScripts import troopBaseScripts
from smart_module.smart_modules.center.CenterEnhanceProsperity import centerEnhanceProsperity
from smart_module.smart_modules.center.CenterEnhanceRents import centerEnhanceRents
from smart_module.smart_modules.center.InitCenterBuilding import initCenterBuilding
from smart_module.smart_modules.center.PatrolGuardParty import patrolGuardParty
from smart_module.smart_modules.center.WecomeEnterCenter import wecomeEnterCenter
from smart_module.smart_modules.center.village.FarmerLive import farmerLive
from smart_module.smart_modules.center.village.VillageBasicBusiness import villageBasicBusiness
from smart_module.smart_modules.develop.SuperFunction import superFunction
from smart_module.smart_modules.develop.TestMode import testMode
from smart_module.smart_modules.map.SurvivalInTheWild import survivalInTheWild
from smart_module.smart_modules.party.LadiesGoOut import ladiesGoOut
from smart_module.smart_modules.party.LordCollectionRents import lordCollectionRents
from smart_module.smart_modules.party.LordSoldiersManage import lordSoldiersManage
from smart_module.smart_modules.party.ShowLordReputationType import showLordReputationType


## 编译环境级别：
## 环境变量 > 配置文件
activeProfile = "dev"


## 基础模块
baseModules = [

    aiBaseScripts,
    partyBaseScripts,
    factionBaseScripts,
    troopBaseScripts,
    missionBaseScripts,
    stringBaseScripts,
]

## 开发模块
devModules = [
    testMode,
    superFunction,
]

smartModuleConfig = {
    ## 开发环境
    "dev":{
        ## 编译后的文件保存到哪里（注意：文件分隔符结尾）
        "exportDir":"../../build//",
        ## 是否跳过原生编译，用于测试自己代码，减少编译时间
        "skipNative":False,
        ## 需要编译的smart module模块集合（有先后顺序）
        "smartModules":mergeList(baseModules,devModules,[
            ## 功能模块
            lordSoldiersManage,
            lordCollectionRents,
            showLordReputationType,
            centerEnhanceProsperity,
            ladiesGoOut,
            centerEnhanceRents,
            initCenterBuilding,
            patrolGuardParty,
            villageBasicBusiness,
            farmerLive,
        ])
    },
    ## 测试环境
    "test":{
        ## D:\Program Files (x86)\Mount&Blade Warband\Modules\Native\
        "exportDir":"D:\\Program Files (x86)\\Mount&Blade Warband\\Modules\\Native\\",
        "smartModules":mergeList(
            baseModules,
            devModules,
        [
            ## 功能模块
            lordSoldiersManage,
            lordCollectionRents,
            showLordReputationType,
            centerEnhanceProsperity,
            ladiesGoOut,
            centerEnhanceRents,
            initCenterBuilding,
            patrolGuardParty,
            villageBasicBusiness,
            survivalInTheWild,
            farmerLive,
            wecomeEnterCenter
        ])
    },
    ## 正式环境
    "prod":{
        "exportDir":"D:\\Program Files (x86)\\Mount&Blade Warband\\Modules\\Native\\",
        "smartModules":mergeList(baseModules,[
            ## 功能模块
            lordSoldiersManage,
            lordCollectionRents,
            showLordReputationType,
            centerEnhanceProsperity,
            ladiesGoOut,
            centerEnhanceRents,
            initCenterBuilding,
            patrolGuardParty,
            villageBasicBusiness,
        ])
    }
}