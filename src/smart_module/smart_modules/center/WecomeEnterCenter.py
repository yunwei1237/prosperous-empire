from smart_module.module_system.header_operations import *
from smart_module.module_system.header_parties import *

wecomeEnterCenter={
    "name":"WecomeEnterCenter",
    "enable":True,
    "version": "v2.0.0",
    "desc":'''
        欢迎功能
        当玩家进入城镇，城堡和村庄时显示欢迎进入。
    ''',
    "actions":[
        ## OnEnterCenter是一个预定义的命令，当玩家进入到据点时调用方括号中的脚本
        ("OnEnterCenter",[
            ## 保存当前城镇的名称
            (str_store_party_name,s1,"$current_town"),
            ## 显示欢迎玩家进入xxx城镇
            (display_message,"@wellcome to {s1}"),
        ]),
    ],
    ## 汉化
    "internationals":{
            "cns":{
                "quick_strings":[
                    ## 将字符串进行汉化
                    "qstr_wellcome_to_{s1}|欢 迎 来 到 {s1} ！",
                ]
            }
    }
}