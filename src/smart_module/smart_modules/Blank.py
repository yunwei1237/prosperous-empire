## 用于编写一些ai方面的功能
from smart_module.module_system.header_operations import *
from smart_module.module_system.header_parties import *
from smart_module.module_system.module_constants import *
from smart_module.smart_core.base.meta_command.meta_command_processor import AppendProcessor

Blank={
    "name":"AiBaseScripts",
    "enable":True,
    "version": "v2.0.0",
    "desc":"这里是描述信息",
    "commands":{
        "AddScript":{
            "target":"scripts",
            "selector":"#last",
            "processor":AppendProcessor,
            "desc": "添加一个在scripts列表中添加一个脚本"
        }
    },
    "actions":[
        ("Append@scripts",[
            ## 代码功能
        ]),
        ("Prepend@scripts",[]),
        ("Delete@scripts",[]),
        ("Replace@scripts",[
            ## 集合中只能有一个元素，否则会报错（替换只能精准替换，不能全部替换，而且，只能使用一个数据替换）
        ])
    ],
    ## 汉化
    "internationals":{
            "cns":{
                "dialogs":[

                ]
            }
    }
}