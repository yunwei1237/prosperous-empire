from smart_module.smart_modules.map.SurvivalInTheWild import initSurvivalTypes

if __name__ == '__main__':
    datas = initSurvivalTypes()

    gameMenuOptionList = datas.get("gameMenuOptionList")
    gameStringsList = datas.get("gameStringsList")