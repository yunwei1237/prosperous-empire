import os

from smart_module.config import smartModuleConfig, activeProfile
from smart_module.smart_core.base.exception import SmartModuleExceptin


################################################
###
### 配置解析器：解析器
### 1.支持多环境
### 2.提供默认配置
### 3.以后支持命令行（optparse）
###
################################################

class SmartModuleConfigParser:
    def __init__(self,profile = "dev",smartModuleConfig = []):
        self.profile = profile
        self.smartModuleConfig = smartModuleConfig
        self.currentConfig = smartModuleConfig.get(profile)
        if self.currentConfig == None:
            raise SmartModuleExceptin("未找到环境配置，请检查：" + str(profile))
    def __str__(self):
        return "smart module 配置解析器"
    def getConfig(self,key,default=None):
        value = self.currentConfig.get(key)
        return value if value != None else default

    def getProfile(self):
        '''
            获得输出目录
        :return:
        '''
        return self.profile

    def getExportDir(self):
        '''
            获得输出目录
        :return:
        '''
        return self.getConfig("exportDir")

    def getSrcDir(self):
        '''
            获得源代（Native）码位置
            默认为：module_system
        :return:
        '''
        return self.getConfig("srcDir","./module_system//")

    def getSmartModules(self):
        '''
            获得全部smart模块
        :return:
        '''
        return self.getConfig("smartModules")

    def getSkipNative(self):
        '''
            是否跳过原始（Native）编译
            默认：不跳过
        :return:
        '''
        return self.getConfig("skipNative",False)


profile = os.getenv("profile")
currentProfile = profile if profile != None else activeProfile
## 全局配置解析器
configParser = SmartModuleConfigParser(currentProfile,smartModuleConfig)

if __name__ == '__main__':
    print(configParser.getConfig("exportDir"))